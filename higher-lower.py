#importing module
import random
#tuples
suits = ('Hearts', 'Diamonds', 'Spades', 'Clubs')
ranks = ('Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Jack', 'Queen', 'King', 'Ace')
#dictionary
values = {'Two':2, 'Three':3, 'Four':4, 'Five':5, 'Six':6, 'Seven':7, 'Eight':8, 
            'Nine':9, 'Ten':10, 'Jack':10, 'Queen':10, 'King':10, 'Ace':11}
class Card():
    """
    Class for creating a single card object
    """
    def __init__(self,suit,rank):
        self.suit = suit
        self.rank = rank
        #looks at the dictionary to determine the value
        self.value = values[rank]
    
    def __str__(self):
        return self.rank + " of " + self.suit

class Deck():
    """
    Class for creating a deck object, which uses the Card class
    """
    def __init__(self):
        self.deck = []
        for suit in suits:
            for rank in ranks:
                #create card object
                created_card = Card(suit,rank)
                self.deck.append(created_card)

    def shuffle(self):
        """
        Uses the random module to shuffle the deck
        """
        random.shuffle(self.deck) #remember - this happens in place!

    def deal(self):
        """
        Takes a card, removes it from the list
        """
        return self.deck.pop()
    
def configureAce():
    """
    Function for configuring the value of Aces to be high or low
    """
    while True:
        print("Do you want the value of Aces high (11) or low (1)?")
        acecheck = input("Press H for high or L for low and then press the enter key: ").upper()
        if acecheck == 'H':
            print('The value of Aces will be 11.')
            values['Ace'] = 11
            print(f'The value of Aces has been set to {values["Ace"]}')
            break
        elif acecheck == 'L':
            print('The value of Aces will be 1.')
            values['Ace'] = 1
            print(f'The value of Aces has been set to {values["Ace"]}')
            break
        else:
            print(f"I don't understand {acecheck} - please try again!")

def playerGuess():
    """
    Lets the player guess if the next card is higher or lower
    """
    while True:
        choice = input('Do you think the next card will be (H)igher or (L)ower? ').upper()
        if choice == 'H':
            print('Higher!')
            return choice
        elif choice == 'L':
            print('Lower!')
            return choice
        else:
            print("I don't understand that! Try again!")

def pickWinner(choice, score):
    """
    By using the player's choice, the outcome will be decided based on the two cards
    """
    if choice == 'H':
        if nextcard.value > dealtcard.value:
            print("You win!")
            score += 1
            return score
        elif nextcard.value == dealtcard.value:
            print('Draw! Both values are the same!')
            return score
        else:
            print("You lose!")
            return score
    elif choice == 'L':
        if nextcard.value < dealtcard.value:
            print("You win!")
            score += 1
            return score
        elif nextcard.value == dealtcard.value:
            print('Draw! Both values are the same!')
            return score
        else:
            print("You lose!")
            return score

#game setup
print("Welcome to higher or lower!")
configureAce()
score = 0

#game logic
while True:
    deck = Deck()
    deck.shuffle()
    dealtcard = deck.deal()
    print(f'Dealing a card! It is {dealtcard}')
    choice = playerGuess()
    print('Here comes the next card....')
    nextcard = deck.deal()
    print(f"It is {nextcard}")
    score = pickWinner(choice, score)
    print(f"Your current score is: {score}")
    playagain = input("Do you want to play again? Type Y for yes or any other key for No ").upper()
    if playagain == 'Y':
        print("Let's play again!")
        pass
    else:
        print('See you later!')
        break

